<?php

/**
 * @file
 * Contains metatag_user_role.module.
 */

use Drupal\metatag\MetatagDefaultsInterface;
use Drupal\metatag\Form\MetatagDefaultsForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\user\Entity\Role;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function metatag_user_role_form_metatag_defaults_add_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $entity_types = MetatagDefaultsForm::getSupportedEntityTypes();
  if (isset($entity_types['user']) && $role_options = \Drupal::service('metatag_user_role')->getOptions()) {
    $label = $entity_types['user'];
    $options = &$form['id']['#options'];
    $options[$label] = isset($options[$label]) ? $options[$label] : [];
    $options[$label] += $role_options;
  }
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function metatag_user_role_metatag_defaults_presave(MetatagDefaultsInterface $metatag_defaults) {
  // Process label for Metatag defaults.
  if ($metatag_defaults->isNew()) {
    $type_parts = explode('__', $metatag_defaults->id());
    if ($type_parts[0] == 'user' && isset($type_parts[2]) && $role = Role::load($type_parts[2])) {
      $label = $metatag_defaults->label();
      $label = \Drupal::service('metatag_user_role')->getLabel($label, $role->label());
      $metatag_defaults->set('label', $label);
    }
  }
}

/**
 * Implements hook_metatags_alter().
 */
function metatag_user_role_metatags_alter(&$metatags, &$context) {
  // Check that the metatags exist and context has entity of user type.
  $entity = isset($context['entity']) ? $context['entity'] : NULL;
  if (empty($entity) || !is_array($metatags) || !$entity instanceof UserInterface) {
    return;
  }

  // Load default tags for a priority user role.
  $role_metatags = \Drupal::service('metatag_user_role')->getTags($entity);
  if (empty($role_metatags)) {
    return;
  }

  // Load the tags specified directly in the entity.
  $entity_metatags = \Drupal::service('metatag.manager')->tagsFromEntity($entity);
  foreach ($role_metatags as $tag => $data) {
    // Replace tags that are not specified in the entity.
    if (!isset($entity_metatags[$tag])) {
      $metatags[$tag] = $data;
    }
  }
}
